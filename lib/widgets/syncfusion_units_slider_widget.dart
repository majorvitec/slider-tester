import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class SyncfusionUnitsSliderWidget extends StatefulWidget {
  const SyncfusionUnitsSliderWidget({Key? key}) : super(key: key);

  @override
  _SyncfusionUnitsSliderWidgetState createState() =>
      _SyncfusionUnitsSliderWidgetState();
}

class _SyncfusionUnitsSliderWidgetState
    extends State<SyncfusionUnitsSliderWidget> {
  final double _moneyMin = 0.0;
  double _moneyMax = 999.0;
  double _moneyValue = 25.0;
  double _moneyInterval = 100;

  final List<String> _dropDownUnits = ['g', 'kg', 'ml', 'l'];
  late List<DropdownMenuItem<String>> _dropDownUnitItems = [];
  String _chosenValue = 'g';

  @override
  void initState() {
    _dropDownUnitItems = _dropDownUnits
        .map((String value) => DropdownMenuItem<String>(
              value: value,
              child: Center(child: Text(value)),
            ))
        .toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              _moneyValue.toStringAsFixed(0) + _chosenValue,
              style: const TextStyle(fontSize: 40.0),
            ),
            Container(
              width: 50.0,
              height: 30.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(color: Colors.blue),
              ),
              child: DropdownButton(
                isExpanded: true,
                value: _chosenValue,
                items: _dropDownUnitItems,
                underline: const SizedBox(),
                onChanged: (String? value) {
                  setState(() {
                    print("value: ${value}");
                    if (value != null) {
                      _chosenValue = value;
                      if (value == 'g' || value == 'ml') {
                        _moneyMax = 999;
                        _moneyInterval = 100;
                      } else if (value == 'kg' || value == 'l') {
                        _moneyMax = 100;
                        _moneyInterval = 10;
                      }
                    }
                  });
                },
              ),
            )
          ],
        ),
        SfSliderTheme(
          data: SfSliderThemeData(
            tickOffset: const Offset(0.0, 10.0),
            tickSize: const Size(3.0, 12.0),
            minorTickSize: const Size(3.0, 8.0),
            activeTickColor: Colors.blue,
            activeMinorTickColor: Colors.blue,
            thumbRadius: 8.0,
            thumbColor: Colors.white,
            thumbStrokeColor: Colors.blue,
            thumbStrokeWidth: 2.0,
          ),
          child: SfSlider(
            min: _moneyMin,
            max: _moneyMax,
            value: _moneyValue.round(),
            interval: _moneyInterval,
            stepSize: 5,
            minorTicksPerInterval: 1,
            inactiveColor: Colors.blue.shade100,
            showDividers: true,
            enableTooltip: true,
            tooltipShape: const SfPaddleTooltipShape(),
            showTicks: true,
            showLabels: true,
            onChanged: (dynamic newValue) {
              setState(() {
                _moneyValue = newValue;
              });
            },
          ),
        ),
      ],
    );
  }
}
