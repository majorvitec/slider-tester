import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class SleekCircularUnitsSliderWidget extends StatefulWidget {
  const SleekCircularUnitsSliderWidget({Key? key}) : super(key: key);

  @override
  _SleekCircularUnitsSliderWidgetState createState() =>
      _SleekCircularUnitsSliderWidgetState();
}

class _SleekCircularUnitsSliderWidgetState
    extends State<SleekCircularUnitsSliderWidget> {
  final double _unitsMin = 0;
  double _unitsMax = 999;
  double _unitsValue = 250;
  String _chosenValue = 'g';

  final List<String> _dropDownUnits = ['g', 'kg', 'ml', 'l'];
  late List<DropdownMenuItem<String>> _dropDownUnitItems = [];

  @override
  void initState() {
    _dropDownUnitItems = _dropDownUnits
        .map(
          (String value) => DropdownMenuItem<String>(
            value: value,
            child: Center(child: Text(value)),
          ),
        )
        .toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SleekCircularSlider(
            appearance: CircularSliderAppearance(
              customWidths: CustomSliderWidths(
                progressBarWidth: 10,
                handlerSize: 2.0,
                shadowWidth: 15.0,
                trackWidth: 6.0,
              ),
              customColors: CustomSliderColors(
                trackColor: Colors.amber.shade200,
                dotColor: Colors.black,
                progressBarColors: [
                  Colors.amber.shade100,
                  Colors.amber,
                  Colors.amber.shade100,
                ],
              ),
              infoProperties: InfoProperties(
                mainLabelStyle: TextStyle(
                  fontSize: 30.0,
                  color: Colors.amber.shade700,
                  fontWeight: FontWeight.bold,
                ),
                bottomLabelStyle: TextStyle(
                  fontSize: 16.0,
                  color: Colors.amber.shade700,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 3.0,
                ),
                modifier: (double a) {
                  return _unitsValue.toStringAsFixed(0) + ' ' + _chosenValue;
                },
              ),
            ),
            min: _unitsMin,
            max: _unitsMax,
            initialValue: _unitsValue,
            onChange: (double newValue) {
              setState(() {
                _unitsValue = newValue;
              });
            },
          ),
          Container(
            width: 50.0,
            height: 30.0,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.amber),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: DropdownButton<String>(
              isExpanded: true,
              value: _chosenValue,
              underline: const SizedBox(),
              style: const TextStyle(
                color: Colors.black,
              ),
              items: _dropDownUnitItems,
              onChanged: (String? value) {
                setState(() {
                  if (value != null) {
                    _chosenValue = value;
                    if (_chosenValue == 'g' || _chosenValue == 'ml') {
                      _unitsMax = 999.0;
                      _unitsValue = 250.0;
                    } else if (_chosenValue == 'kg' || _chosenValue == 'l') {
                      _unitsMax = 100.0;
                      _unitsValue = 25.0;
                    }
                  }
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
